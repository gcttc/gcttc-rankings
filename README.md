# Guayaquil City Table Tennis Club - Rankings Tool

<!-- TOC -->
* [Guayaquil City Table Tennis Club - Rankings Tool](#guayaquil-city-table-tennis-club---rankings-tool)
  * [Overview](#overview)
    * [Algorithm](#algorithm)
  * [Configuration](#configuration)
  * [Technical documentation](#technical-documentation)
    * [Entity Relation Diagram (ERD)](#entity-relation-diagram-erd)
    * [Domain Model Class Diagram](#domain-model-class-diagram)
    * [Architecture (Hexagonal)](#architecture-hexagonal)
<!-- TOC -->
<br/>

> This repository is one of a collection of three that support this solution:
>
> - The [GCTTC Rankings Tool] repository: The program used for the calculation.
> - The [GCTTC Rankings Data] repository: The initial and tournament reported data.
> - The [GCTTC Rankings Web] repository: The calculated data presentation web application.

## Overview

This is the repository of the **GCTTC Rankings Tool**. This is a tool built for calculation and reporting the Table
Tennis ranking held by [Guayaquil City Table Tennis Club].

The main functionality of the tool is described in the next diagram:

```plantuml

skinparam defaultTextAlignment center

agent "**GCTTC Ranking Tool**" as tool
folder "initial-ranking\n\n<**initial-ranking.csv**>" as initial
folder "tournaments\n\n<tournament1.txt>\n<tournament2.txt>\n...\n<tournamentN.txt>" as tournaments
database "GCTTC Ranking\ndatabase" as db
folder "output\n\n<**rankings.json**>" as output

tool -l-> initial : "1. Read initial\nranking data"
tool -u-> tournaments : "2. Read each\ntournament report"
tool -d-> db : "3. Save ranking data\nfrom initial ranking\nand new rankings after\neach tournament match"
tool -r-> output : "4. Export calculated\nranking data in JSON format"

```

### Algorithm

The algorithm used for the new ranking calculation is a simplified version of
the [USA Table Tennis Rating System]. More information about this could be found in the [USATT Ratings Explained]
article.

The main class which implements it logic is the [USATTRankingCalculator.java] class. And all the related tests about
this algorithm could be found in the [USATTRankingCalculatorTest.java] class.

## Configuration

The following properties could be configured through
the [application.properties] file:

| Property                        | Description                                                                                                                     | Default value              |
|---------------------------------|---------------------------------------------------------------------------------------------------------------------------------|----------------------------|
| `process.initial-ranking-file`  | This is the CSV file containing the most updated ranking.<br/>This is used as a starting point for all the calculation process. | `data/initial-ranking.csv` |
| `process.tournaments-directory` | This is the folder from which all the tournament data files will be read.                                                       | `data/tournaments`         |
| `process.output-directory`      | This is the folder where all the calculated ranking data will be exported.                                                      | `data/output`              |

## Technical documentation

### Entity Relation Diagram (ERD)

This is the entity model that is implemented in the in-memory database through the process.

```plantuml
' hide the spot
hide circle

' avoid problems with angled crows feet
skinparam linetype ortho

entity "Player" as player {
  * id : uuid <<generated>>
  --
  * name : varchar(100)
  * lastname : varchar(100)
}

entity "Tournament" as tournament {
  * id : uuid <<generated>>
  --
  * name : varchar(200)
  * organizer : varchar(200)
  * date : date
}

entity "Match" as match {
  * id : uuid <<generated>>
  --
  * tournament_id : uuid <<FK>>
  * player_a_id : uuid <<FK>>
  * player_a_won_sets : number
  * player_b_id : uuid <<FK>>
  * player_b_won_sets : number
}

entity "Ranking" as ranking {
  * id : uuid <<generated>>
  --
  updated_at : date
  last_match_id : uuid
  * player_id : uuid <<FK>>
  player_ranking_before_match : number
  * player_ranking_after_match : number
  current_ranking : boolean
}

player ||..|{ match
match }|..|| tournament
player ||..|{ ranking
```

### Domain Model Class Diagram

This is the rich domain model that is implemented through the process.

```plantuml

class Player {
   - id : UUID
   - name : String
   - lastname : String
}

class Tournament {
   - id : UUID
   - name : String
   - organizer : String
   - date : LocalDate
   - matches : List<Match>
}

class Match {
   - id : UUID
   - tournament : Tournament
   - playerA : Player
   - playerAWonSets : Short
   - playerB : Player
   - playerBWonSets : Short
   + Player getWinner()
   + Player getLoser()
}

class Ranking {
   - id : UUID
   - updatedAt : LocalDate
   - lastMatchID : UUID
   - player : Player
   - playerRankingBeforeMatch : Integer
   - playerRankingAfterMatch : Integer
   - currentRanking : boolean
   + Integer addRecord(Match match)
}

class RankingReport {
    - date : LocalDateTime
    - rankings : List<Ranking>
}

Player --o Match
Match o--o Tournament
Player --o Ranking
Ranking --o RankingReport

```

### Architecture (Hexagonal)

The source code of this program follows an [Hexagonal Architecture] pattern.


[Guayaquil City Table Tennis Club]: https://www.instagram.com/guayaquilcitytabletennisclub

[GCTTC Rankings Tool]: https://gitlab.com/gcttc/gcttc-rankings

[GCTTC Rankings Data]: https://gitlab.com/gcttc/gcttc-rankings-data

[GCTTC Rankings Web]: https://gitlab.com/gcttc/gcttc-rankings-web

[USA Table Tennis Rating System]: https://www.usatt.org/events-and-results/rating-systems-explained

[USATT Ratings Explained]: https://racketinsight.com/table-tennis/usatt-ratings-explained/

[USATTRankingCalculator.java]: src/main/java/com/gcttc/gcttcrankings/domain/services/USATTRankingCalculator.java

[USATTRankingCalculatorTest.java]: src/test/java/com/gcttc/gcttcrankings/domain/services/USATTRankingCalculatorTest.java

[application.properties]: src/main/resources/application.properties

[Hexagonal Architecture]: https://alistair.cockburn.us/hexagonal-architecture