package com.gcttc.gcttcrankings.infra.adapters.driven.rankings.sqldb;

import com.gcttc.gcttcrankings.domain.Player;
import com.gcttc.gcttcrankings.domain.Ranking;
import com.gcttc.gcttcrankings.domain.Tournament;
import com.gcttc.gcttcrankings.infra.adapters.driver.console.Startup;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.Clock;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class RankingSQLDBRepositoryITTest {

    @Autowired
    private RankingSQLDBRepository instance;

    @MockBean
    private Startup startup;

    @Test
    void whenSendingValidRankingData_shouldSaveTheData() {

        Ranking
                givenRanking =
                Ranking.buildRankingAs(UUID.randomUUID(),
                        LocalDate.now(Clock.systemDefaultZone()),
                        UUID.randomUUID(),
                        new Player("Any name", "Any lastname"),
                        1250,
                        1255,
                        true);

        instance.saveRanking(givenRanking);

        List<Ranking> savedRankings = instance.findRankingsByPlayerId(givenRanking.getPlayer().getId());
        assertEquals(1, savedRankings.size());

        Ranking savedRanking = instance.findLastRankingByPlayerId(givenRanking.getPlayer().getId());

        assertEquals(givenRanking.getId(), savedRanking.getId());
        assertEquals(givenRanking.getUpdatedAt(), savedRanking.getUpdatedAt());
        assertEquals(givenRanking.getLastMatchID(), savedRanking.getLastMatchID());
        assertEquals(givenRanking.getPlayer().getName(), savedRanking.getPlayer().getName());
        assertEquals(givenRanking.getPlayer().getLastname(), savedRanking.getPlayer().getLastname());
        assertEquals(givenRanking.getPlayerRankingBeforeMatch(), savedRanking.getPlayerRankingBeforeMatch());
        assertEquals(givenRanking.getPlayerRankingAfterMatch(), savedRanking.getPlayerRankingAfterMatch());
        assertTrue(savedRanking.isCurrentRanking());

    }

    @Test
    void whenSendingValidRankingListData_shouldSaveTheData() {

        Player anyPlayer = new Player("Any name", "Any lastname");
        Player anyOtherPlayer = new Player("Any other name", "Any other lastname");

        List<Ranking> givenRankings = new ArrayList<>();
        givenRankings.add(Ranking.buildFirstDefaultRankingFor(LocalDate.now(Clock.systemDefaultZone()),
                anyPlayer));
        givenRankings.add(Ranking.buildFirstDefaultRankingFor(LocalDate.now(Clock.systemDefaultZone()),
                anyOtherPlayer));

        instance.saveRankings(givenRankings);

        List<Ranking> savedRankings = instance.findAllRankings();
        assertTrue(savedRankings.size() >= 2);

        long savedRankingsCount = instance.countAllRankings();
        assertEquals(savedRankings.size(), savedRankingsCount);

        List<Ranking> savedAnyPlayerRanking = instance.findRankingsByPlayerId(anyPlayer.getId());
        List<Ranking> savedAnyOtherPlayerRanking = instance.findRankingsByPlayerId(anyOtherPlayer.getId());

        assertEquals(givenRankings.get(0).getId(), savedAnyPlayerRanking.get(0).getId());
        assertEquals(givenRankings.get(0).getUpdatedAt(), savedAnyPlayerRanking.get(0).getUpdatedAt());
        assertEquals(givenRankings.get(0).getLastMatchID(), savedAnyPlayerRanking.get(0).getLastMatchID());
        assertEquals(givenRankings.get(0).getPlayer().getName(), savedAnyPlayerRanking.get(0).getPlayer().getName());
        assertEquals(givenRankings.get(0).getPlayer().getLastname(), savedAnyPlayerRanking.get(0).getPlayer().getLastname());
        assertEquals(givenRankings.get(0).getPlayerRankingBeforeMatch(), savedAnyPlayerRanking.get(0).getPlayerRankingBeforeMatch());
        assertEquals(givenRankings.get(0).getPlayerRankingAfterMatch(), savedAnyPlayerRanking.get(0).getPlayerRankingAfterMatch());
        assertEquals(givenRankings.get(0).isCurrentRanking(), savedAnyPlayerRanking.get(0).isCurrentRanking());

        assertEquals(givenRankings.get(1).getId(), savedAnyOtherPlayerRanking.get(0).getId());
        assertEquals(givenRankings.get(1).getUpdatedAt(), savedAnyOtherPlayerRanking.get(0).getUpdatedAt());
        assertEquals(givenRankings.get(1).getLastMatchID(), savedAnyOtherPlayerRanking.get(0).getLastMatchID());
        assertEquals(givenRankings.get(1).getPlayer().getName(), savedAnyOtherPlayerRanking.get(0).getPlayer().getName());
        assertEquals(givenRankings.get(1).getPlayer().getLastname(), savedAnyOtherPlayerRanking.get(0).getPlayer().getLastname());
        assertEquals(givenRankings.get(1).getPlayerRankingBeforeMatch(), savedAnyOtherPlayerRanking.get(0).getPlayerRankingBeforeMatch());
        assertEquals(givenRankings.get(1).getPlayerRankingAfterMatch(), savedAnyOtherPlayerRanking.get(0).getPlayerRankingAfterMatch());
        assertEquals(givenRankings.get(1).isCurrentRanking(), savedAnyOtherPlayerRanking.get(0).isCurrentRanking());

    }

    @Test
    void whenSendingValidTournamentData_shouldSaveTheData() {

        LocalDate anyDate = LocalDate.now(Clock.systemDefaultZone());
        Tournament givenTournament = new Tournament("Any tournament name", "Any organizer name", anyDate);

        instance.saveTournament(givenTournament);

        List<Tournament> savedTournaments = instance.findAllTournaments();
        assertEquals(1, savedTournaments.size());

        long savedTournamentsCount = instance.countAllTournaments();
        assertEquals(savedTournaments.size(), savedTournamentsCount);

    }

    @Test
    void whenLookingForNonExistentPlayer_shouldReturnNull() {

        Player result = instance.findPlayerByNameAndLastname("anyName", "anyLastname");

        assertNull(result);

    }

    //TODO Add tests saving Matches and tournaments

}