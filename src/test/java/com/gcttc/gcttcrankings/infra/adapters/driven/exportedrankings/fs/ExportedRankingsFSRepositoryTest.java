package com.gcttc.gcttcrankings.infra.adapters.driven.exportedrankings.fs;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.TextNode;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.gcttc.gcttcrankings.domain.LastMatchReportItem;
import com.gcttc.gcttcrankings.domain.RankingReport;
import com.gcttc.gcttcrankings.domain.RankingReportItem;
import com.gcttc.gcttcrankings.domain.exceptions.ExportRankingException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Clock;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertInstanceOf;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;

class ExportedRankingsFSRepositoryTest {

    private static ObjectMapper mapper;

    @BeforeAll
    static void setUp() {
        mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
    }

    @Test
    void whenExistingRankingInfo_shouldGenerateTheJsonFile(@TempDir Path tempDir)
            throws IOException {

        Path tempDirPath = tempDir.resolve("output");
        ExportedRankingsFSRepository instance = new ExportedRankingsFSRepository(tempDirPath.toString());

        LocalDate anyRankingDate = LocalDate.now(Clock.systemUTC());

        List<RankingReportItem> givenRankings = new ArrayList<>();
        givenRankings.add(new RankingReportItem(anyRankingDate,
                null,
                UUID.randomUUID(),
                "Any name",
                "Any lastname",
                1400,
                1410,
                true));
        givenRankings.add(new RankingReportItem(anyRankingDate,
                null,
                UUID.randomUUID(),
                "Any name 2",
                "Any lastname 2",
                1400,
                1390,
                true));
        givenRankings.add(new RankingReportItem(anyRankingDate,
                new LastMatchReportItem("Any tournament name",
                        "Any opponent name",
                        (short) 2,
                        1400,
                        1410,
                        (short) 1),
                UUID.randomUUID(),
                "Any name 2",
                "Any lastname 2",
                1400,
                1390,
                true));


        LocalDateTime anyReportDateTime = LocalDateTime.now(Clock.systemDefaultZone());
        RankingReport rankingReport = new RankingReport(anyReportDateTime, givenRankings);

        instance.save(rankingReport);

        File generatedFile = tempDirPath.resolve("rankings.json").toFile();
        assertTrue(generatedFile.exists());
        JsonNode nodeTree = mapper.readTree(generatedFile);
        assertInstanceOf(TextNode.class, nodeTree.get("date"));
        assertInstanceOf(TextNode.class, nodeTree.get("rankings").get(0).get("updatedAt"));

        RankingReport generatedReport = mapper.readValue(generatedFile, RankingReport.class);

        assertEquals(3, generatedReport.getRankings().size());
        assertEquals(anyReportDateTime, generatedReport.getDate());

    }

    @Test
    void whenExistingRankingInfo_shouldGenerateTheJsonFile() {

        ExportedRankingsFSRepository instance = new ExportedRankingsFSRepository("//");

        try (MockedStatic<Files> files = Mockito.mockStatic(Files.class)) {

            files.when(() -> Files.createDirectories(any(Path.class)))
                 .thenThrow(new IOException("Any IO exception"));

            RankingReport
                    rankingReport =
                    new RankingReport(LocalDateTime.now(Clock.systemDefaultZone()), new ArrayList<>());

            assertThrows(ExportRankingException.class, () -> instance.save(rankingReport));

        }

    }

}