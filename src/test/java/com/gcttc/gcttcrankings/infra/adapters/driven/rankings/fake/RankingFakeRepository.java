package com.gcttc.gcttcrankings.infra.adapters.driven.rankings.fake;

import com.gcttc.gcttcrankings.domain.Match;
import com.gcttc.gcttcrankings.domain.Player;
import com.gcttc.gcttcrankings.domain.Ranking;
import com.gcttc.gcttcrankings.domain.Tournament;
import com.gcttc.gcttcrankings.domain.ports.driven.RankingRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class RankingFakeRepository
        implements RankingRepository {

    private final List<Player> players = new ArrayList<>();
    private final List<Match> matches = new ArrayList<>();
    private final List<Tournament> tournaments = new ArrayList<>();
    private final List<Ranking> rankings = new ArrayList<>();

    @Override
    public void saveRankings(List<Ranking> rankingEntries) {
        rankings.addAll(rankingEntries);
    }

    @Override
    public void saveRanking(Ranking ranking) {
        //Integrity emulation
        if (ranking.getLastMatchID() != null)
            matches.stream().filter(m -> m.getId().equals(ranking.getLastMatchID())).findAny().orElseThrow();

        //Find previous ranking, delete if exist
        rankings.stream().filter(r -> r.getId().equals(ranking.getId())).findAny().ifPresent(rankings::remove);

        //Save operation
        rankings.add(ranking);
    }

    @Override
    public List<Ranking> findAllRankings() {
        return rankings;
    }

    @Override
    public Long countAllRankings() {
        return (long) rankings.size();
    }

    @Override
    public Ranking findLastRankingByPlayerId(UUID playerID) {
        return rankings.stream()
                       .filter(r -> r.getPlayer().getId().equals(playerID))
                       .reduce((first, second) -> second)
                       .orElse(null);
    }

    @Override
    public List<Ranking> findRankingsByPlayerId(UUID playerID) {
        return rankings.stream().filter(r -> r.getPlayer().getId().equals(playerID)).toList();
    }

    @Override
    public Ranking findPairRankingByRanking(Ranking ranking) {
        return null;
    }

    @Override
    public void saveTournament(Tournament tournament) {
        tournaments.add(tournament);
    }

    @Override
    public List<Tournament> findAllTournaments() {
        return tournaments;
    }

    @Override
    public Long countAllTournaments() {
        return (long) tournaments.size();
    }

    @Override
    public void saveMatch(Match match) {
        //Integrity emulation
        players.stream().filter(p -> p.getId().equals(match.getPlayerA().getId())).findAny().orElseThrow();
        players.stream().filter(p -> p.getId().equals(match.getPlayerB().getId())).findAny().orElseThrow();
        //Save operation
        matches.add(match);
    }

    @Override
    public Long countAllMatches() {
        return (long) matches.size();
    }

    @Override
    public List<Match> findMatchesByTournament(UUID tournamentID) {
        return matches.stream().filter(m -> m.getTournament().getId().equals(tournamentID)).toList();
    }

    @Override
    public Match findMatchById(UUID matchID) {
        return matches.stream().filter(m -> m.getId().equals(matchID)).findAny().orElse(null);
    }

    @Override
    public void savePlayer(Player player) {
        players.add(player);
    }

    @Override
    public Long countAllPlayers() {
        return (long) players.size();
    }

    @Override
    public List<Player> findAllPlayers() {
        return players;
    }

    @Override
    public Player findPlayerByNameAndLastname(String name, String lastname) {
        return players.stream()
                      .filter(p -> p.getName().equals(name) && p.getLastname().equals(lastname))
                      .findFirst()
                      .orElse(null);
    }

}
