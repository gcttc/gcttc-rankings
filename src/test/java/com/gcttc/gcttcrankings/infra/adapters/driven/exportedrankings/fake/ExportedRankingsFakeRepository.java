package com.gcttc.gcttcrankings.infra.adapters.driven.exportedrankings.fake;

import com.gcttc.gcttcrankings.domain.RankingReport;
import com.gcttc.gcttcrankings.domain.ports.driven.ExportedRankingsRepository;

public class ExportedRankingsFakeRepository
        implements ExportedRankingsRepository {

    private RankingReport rankingReport;

    @Override
    public void save(RankingReport rankingReport) {
        this.rankingReport = rankingReport;
    }

    public RankingReport getRankingReport() {
        return rankingReport;
    }
}
