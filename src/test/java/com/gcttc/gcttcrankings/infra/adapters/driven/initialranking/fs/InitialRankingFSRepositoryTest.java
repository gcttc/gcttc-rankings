package com.gcttc.gcttcrankings.infra.adapters.driven.initialranking.fs;

import com.gcttc.gcttcrankings.domain.Ranking;
import com.gcttc.gcttcrankings.domain.exceptions.InitialRankingLoadException;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

class InitialRankingFSRepositoryITTest {

    private InitialRankingFSRepository instance;

    @Test
    void whenReadingAValidFile_shouldReturnTheData()
            throws IOException {

        LocalDate expectedRankingDate = LocalDate.of(2024,11,10);
        Resource inputFile = new ClassPathResource("initial-rankings/initial-ranking-ok-20241110.csv");

        instance = new InitialRankingFSRepository(inputFile.getFile().getAbsolutePath());

        List<Ranking> loadedRankings = instance.loadAllRankings();

        assertEquals(4, loadedRankings.size());

        assertEquals(expectedRankingDate, loadedRankings.get(0).getUpdatedAt());
        assertEquals("AnyName", loadedRankings.get(0).getPlayer().getName());
        assertEquals("AnyLastname", loadedRankings.get(0).getPlayer().getLastname());
        assertNull(loadedRankings.get(0).getPlayerRankingBeforeMatch());
        assertEquals(1752, loadedRankings.get(0).getPlayerRankingAfterMatch());
        assertNull(loadedRankings.get(0).getLastMatchID());

        assertEquals(expectedRankingDate, loadedRankings.get(1).getUpdatedAt());
        assertEquals("AnyName 2", loadedRankings.get(1).getPlayer().getName());
        assertEquals("AnyLastname 2", loadedRankings.get(1).getPlayer().getLastname());
        assertNull(loadedRankings.get(1).getPlayerRankingBeforeMatch());
        assertEquals(1511, loadedRankings.get(1).getPlayerRankingAfterMatch());
        assertNull(loadedRankings.get(1).getLastMatchID());

        assertEquals(expectedRankingDate, loadedRankings.get(2).getUpdatedAt());
        assertEquals("AnyName 3", loadedRankings.get(2).getPlayer().getName());
        assertEquals("AnyLastname 3", loadedRankings.get(2).getPlayer().getLastname());
        assertNull(loadedRankings.get(2).getPlayerRankingBeforeMatch());
        assertEquals(1642, loadedRankings.get(2).getPlayerRankingAfterMatch());
        assertNull(loadedRankings.get(2).getLastMatchID());

        assertEquals(expectedRankingDate, loadedRankings.get(3).getUpdatedAt());
        assertEquals("AnyName 4", loadedRankings.get(3).getPlayer().getName());
        assertEquals("AnyLastname 4", loadedRankings.get(3).getPlayer().getLastname());
        assertNull(loadedRankings.get(3).getPlayerRankingBeforeMatch());
        assertEquals(1457, loadedRankings.get(3).getPlayerRankingAfterMatch());
        assertNull(loadedRankings.get(3).getLastMatchID());

    }

    @Test
    void whenReadingAFileWithInvalidEntries_shouldReturnOnlyTheValidData()
            throws IOException {

        Resource inputFile = new ClassPathResource("initial-rankings/initial-ranking-only2ok-20241110.csv");

        instance = new InitialRankingFSRepository(inputFile.getFile().getAbsolutePath());

        List<Ranking> loadedRankings = instance.loadAllRankings();

        assertEquals(2, loadedRankings.size());

    }

    @Test
    void whenReadingANonExistentFile_shouldThrowInitialRankingLoadException() {

        instance = new InitialRankingFSRepository("initial-rankings/non-existent.csv");

        assertThrows(InitialRankingLoadException.class, () -> instance.loadAllRankings());

    }

}