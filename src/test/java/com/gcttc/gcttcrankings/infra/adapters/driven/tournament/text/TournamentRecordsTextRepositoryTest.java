package com.gcttc.gcttcrankings.infra.adapters.driven.tournament.text;

import com.gcttc.gcttcrankings.domain.TournamentRecord;
import com.gcttc.gcttcrankings.domain.exceptions.TournamentLoadException;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class TournamentRecordsTextRepositoryTest {

    private TournamentRecordsTextRepository instance;

    @Test
    void whenLoadingTournamentRecordsFromFiles_shouldLoadAllData()
            throws IOException {

        Resource tournamentDirectory = new ClassPathResource("tournaments");

        instance = new TournamentRecordsTextRepository(tournamentDirectory.getFile().getAbsolutePath());

        List<TournamentRecord> loadedTournaments = instance.loadAllTournamentRecords();

        assertEquals(2, loadedTournaments.size());

        assertEquals("Any tournament 1", loadedTournaments.get(0).getName());
        assertEquals("Any organizer 1", loadedTournaments.get(0).getOrganizer());
        assertEquals(LocalDate.of(2023, 11, 22), loadedTournaments.get(0).getDate());
        assertEquals(6, loadedTournaments.get(0).getMatches().size());
        assertEquals("Name1", loadedTournaments.get(0).getMatches().get(0).getPlayerA().getName());
        assertEquals("Lastname3", loadedTournaments.get(0).getMatches().get(1).getPlayerB().getLastname());
        assertEquals((short) 3, loadedTournaments.get(0).getMatches().get(2).getPlayerAWonSets());
        assertEquals((short) 2, loadedTournaments.get(0).getMatches().get(3).getPlayerBWonSets());

        assertEquals("Any tournament 2", loadedTournaments.get(1).getName());
        assertEquals("Any organizer 2", loadedTournaments.get(1).getOrganizer());
        assertEquals(LocalDate.of(2023, 12, 8), loadedTournaments.get(1).getDate());
        assertEquals(4, loadedTournaments.get(1).getMatches().size());
        assertEquals("Name1", loadedTournaments.get(1).getMatches().get(0).getPlayerA().getName());
        assertEquals("Lastname4", loadedTournaments.get(1).getMatches().get(1).getPlayerB().getLastname());
        assertEquals((short) 3, loadedTournaments.get(1).getMatches().get(2).getPlayerAWonSets());
        assertEquals((short) 2, loadedTournaments.get(1).getMatches().get(3).getPlayerBWonSets());

    }

    @Test
    void whenReadingANonExistentFile_shouldThrowTournamentRecordLoadException() {

        instance = new TournamentRecordsTextRepository("tournaments/non-existent.csv");

        assertThrows(TournamentLoadException.class, () -> instance.loadAllTournamentRecords());

    }

}