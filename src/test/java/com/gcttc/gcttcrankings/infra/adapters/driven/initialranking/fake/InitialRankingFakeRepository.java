package com.gcttc.gcttcrankings.infra.adapters.driven.initialranking.fake;

import com.gcttc.gcttcrankings.domain.Player;
import com.gcttc.gcttcrankings.domain.Ranking;
import com.gcttc.gcttcrankings.domain.ports.driven.InitialRankingRepository;

import java.time.Clock;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class InitialRankingFakeRepository
        implements InitialRankingRepository {

    @Override
    public List<Ranking> loadAllRankings() {

        LocalDate anyDate = LocalDate.now(Clock.systemUTC());

        List<Ranking> fakeRankings = new ArrayList<>();
        fakeRankings.add(Ranking.buildFirstRankingFor(anyDate, new Player("Any name", "Any lastname"), 1400));
        fakeRankings.add(Ranking.buildFirstRankingFor(anyDate, new Player("Any name 2", "Any lastname 2"), 1200));
        fakeRankings.add(Ranking.buildFirstRankingFor(anyDate, new Player("Any name 3", "Any lastname 3"), 1800));

        return fakeRankings;

    }

}
