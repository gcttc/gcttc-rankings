package com.gcttc.gcttcrankings.infra.adapters.driven.tournaments.fake;

import com.gcttc.gcttcrankings.domain.MatchRecord;
import com.gcttc.gcttcrankings.domain.PlayerRecord;
import com.gcttc.gcttcrankings.domain.TournamentRecord;
import com.gcttc.gcttcrankings.domain.ports.driven.TournamentRecordsRepository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class TournamentRecordsFakeRepository
        implements TournamentRecordsRepository {

    @Override
    public List<TournamentRecord> loadAllTournamentRecords() {

        List<TournamentRecord> tournamentRecords = new ArrayList<>();

        PlayerRecord player1 = new PlayerRecord("Name1", "Lastname1");
        PlayerRecord player2 = new PlayerRecord("Name2", "Lastname2");
        PlayerRecord player3 = new PlayerRecord("Name3", "Lastname3");
        PlayerRecord player4 = new PlayerRecord("Name4", "Lastname4");

        List<MatchRecord> t1MatchRecords = new ArrayList<>();
        t1MatchRecords.add(new MatchRecord(player1, (short) 3, player2, (short) 2));
        t1MatchRecords.add(new MatchRecord(player1, (short) 3, player3, (short) 2));
        t1MatchRecords.add(new MatchRecord(player1, (short) 3, player4, (short) 2));
        t1MatchRecords.add(new MatchRecord(player2, (short) 3, player3, (short) 2));
        t1MatchRecords.add(new MatchRecord(player2, (short) 3, player4, (short) 2));
        t1MatchRecords.add(new MatchRecord(player3, (short) 3, player4, (short) 2));
        tournamentRecords.add(new TournamentRecord("Any tournament 1",
                "Any organizer 1",
                LocalDate.of(2023, 11, 22),
                t1MatchRecords));

        List<MatchRecord> t2MatchRecords = new ArrayList<>();
        t2MatchRecords.add(new MatchRecord(player1, (short) 3, player2, (short) 2));
        t2MatchRecords.add(new MatchRecord(player3, (short) 3, player4, (short) 2));
        t2MatchRecords.add(new MatchRecord(player1, (short) 3, player3, (short) 2));
        t2MatchRecords.add(new MatchRecord(player2, (short) 3, player4, (short) 2));
        tournamentRecords.add(new TournamentRecord("Any tournament 2",
                "Any organizer 2",
                LocalDate.of(2023, 12, 8),
                t2MatchRecords));

        return tournamentRecords;

    }

}
