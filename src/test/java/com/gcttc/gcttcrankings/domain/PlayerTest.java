package com.gcttc.gcttcrankings.domain;

import com.gcttc.gcttcrankings.domain.exceptions.InvalidNameException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

class PlayerTest {

    @Test
    void whenCreatingWithValidName_shouldCreateTheInstance() {

        Player anyPlayer = new Player("Any name", "Any lastname");

        assertNotNull(anyPlayer);
        assertNotNull(anyPlayer.getId());
        assertEquals("Any name", anyPlayer.getName());

    }

    @ParameterizedTest
    @MethodSource("provideInvalidNamesAndLastnames")
    void whenCreatingWithInvalidName_shouldThrowException(String nameValue, String lastnameValue) {

        assertThrows(InvalidNameException.class, () -> new Player(nameValue, lastnameValue));

    }

    private static Stream<Arguments> provideInvalidNamesAndLastnames() {
        return Stream.of(Arguments.of(null, null),
                Arguments.of("", null),
                Arguments.of(" ", null),
                Arguments.of("  ", null),
                Arguments.of("\t", null),
                Arguments.of("\n", null),
                Arguments.of(null, ""),
                Arguments.of("", ""),
                Arguments.of(" ", ""),
                Arguments.of("  ", ""),
                Arguments.of("\t", ""),
                Arguments.of("\n", ""),
                Arguments.of(null, " "),
                Arguments.of("", " "),
                Arguments.of(" ", " "),
                Arguments.of("  ", " "),
                Arguments.of("\t", " "),
                Arguments.of("\n", " "),
                Arguments.of(null, "  "),
                Arguments.of("", "  "),
                Arguments.of(" ", "  "),
                Arguments.of("  ", "  "),
                Arguments.of("\t", "  "),
                Arguments.of("\n", "  "),
                Arguments.of(null, "\t"),
                Arguments.of("", "\t"),
                Arguments.of(" ", "\t"),
                Arguments.of("  ", "\t"),
                Arguments.of("\t", "\t"),
                Arguments.of("\n", "\t"),
                Arguments.of(null, "\n"),
                Arguments.of("", "\n"),
                Arguments.of(" ", "\n"),
                Arguments.of("  ", "\n"),
                Arguments.of("\t", "\n"),
                Arguments.of("\n", "\n"));
    }

}