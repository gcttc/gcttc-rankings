package com.gcttc.gcttcrankings.domain;

import com.gcttc.gcttcrankings.domain.exceptions.InvalidTotalNumberOfSetsException;
import com.gcttc.gcttcrankings.domain.exceptions.SameWinningSetsException;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

class MatchTest {

    private final Tournament anyTournament = new Tournament("anyName", "anyOrganizer", LocalDate.of(2024, 6, 25));
    private final Player anyPlayerA = new Player("Name A", "Lastname A");
    private final Player anyPlayerB = new Player("Name B", "Lastname B");

    @Test
    void whenCreatingWithSetsWithShortDifference_shouldCreateTheInstance() {

        Match anyMatch = new Match(anyTournament, anyPlayerA, (short) 3, anyPlayerB, (short) 2);

        assertNotNull(anyMatch);
        assertNotNull(anyMatch.getId());
        assertNotNull(anyMatch.getTournament());
        assertEquals("Name A", anyMatch.getPlayerA().getName());
        assertEquals((short) 3, anyMatch.getPlayerAWonSets());
        assertEquals("Name B", anyMatch.getPlayerB().getName());
        assertEquals((short) 2, anyMatch.getPlayerBWonSets());

    }

    @Test
    void whenCreatingWithSetsWithLongDifference_shouldCreateTheInstance() {

        Match anyMatch = new Match(anyTournament, anyPlayerA, (short) 3, anyPlayerB, (short) 1);

        assertNotNull(anyMatch);
        assertNotNull(anyMatch.getId());
        assertNotNull(anyMatch.getTournament());
        assertEquals("Name A", anyMatch.getPlayerA().getName());
        assertEquals((short) 3, anyMatch.getPlayerAWonSets());
        assertEquals("Name B", anyMatch.getPlayerB().getName());
        assertEquals((short) 1, anyMatch.getPlayerBWonSets());

    }

    @Test
    void whenCreatingWithSameWinningSets_shouldThrowException() {

        assertThrows(SameWinningSetsException.class,
                () -> new Match(anyTournament, anyPlayerA, (short) 3, anyPlayerB, (short) 3));

    }

    @Test
    void whenCreatingWithInvalidTotalNumberOfSets_shouldThrowException() {

        InvalidTotalNumberOfSetsException
                ex =
                assertThrows(InvalidTotalNumberOfSetsException.class,
                        () -> new Match(anyTournament, anyPlayerA, (short) 0, anyPlayerB, (short) 1));

        assertFalse(ex.getMessage().isBlank());

    }

}