package com.gcttc.gcttcrankings.domain;

import org.junit.jupiter.api.Test;

import java.time.Clock;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

class RankingTest {

    @Test
    void whenCreatingWithValidName_shouldCreateTheInstance() {

        LocalDate anyDate = LocalDate.now(Clock.systemDefaultZone());

        Ranking anyRanking = Ranking.buildFirstRankingFor(anyDate, new Player("Any name", "Any lastname"), 1200);

        assertNotNull(anyRanking);
        assertNotNull(anyRanking.getId());
        assertNull(anyRanking.getLastMatchID());
        assertEquals("Any name", anyRanking.getPlayer().getName());
        assertEquals("Any lastname", anyRanking.getPlayer().getLastname());
        assertNull(anyRanking.getPlayerRankingBeforeMatch());
        assertEquals(1200, anyRanking.getPlayerRankingAfterMatch());

    }

}