package com.gcttc.gcttcrankings.domain.services;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

class USATTRankingCalculatorTest {

    private final USATTRankingCalculator instance = new USATTRankingCalculator();

    /*Test case format: winner previous ranking, loser previous Ranking, expected exchanged points*/
    @ParameterizedTest
    @CsvSource({"1500,1490,8",
            "1500,1480,7",
            "1500,1450,6",
            "1500,1430,5",
            "1500,1400,4",
            "1500,1380,3",
            "1500,1350,2",
            "1500,1330,2",
            "1500,1300,1",
            "1500,1280,1",
            "1500,1250,0",
            "1490,1500,8",
            "1480,1500,10",
            "1450,1500,13",
            "1430,1500,16",
            "1400,1500,20",
            "1380,1500,25",
            "1350,1500,30",
            "1330,1500,35",
            "1300,1500,40",
            "1280,1500,45",
            "1250,1500,50"})
    void matchResultMassiveTest(int winnerLastRanking, int loserLastRanking, int expectedExchangedPoints) {

        Integer winnerNewRanking = instance.calculateWinnerNewRanking(winnerLastRanking, loserLastRanking);
        Integer loserNewRanking = instance.calculateLoserNewRanking(winnerLastRanking, loserLastRanking);

        assertEquals(winnerNewRanking, winnerLastRanking + expectedExchangedPoints);
        assertEquals(loserNewRanking, loserLastRanking - expectedExchangedPoints);

    }

}