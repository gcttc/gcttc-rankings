package com.gcttc.gcttcrankings.domain.services;

import com.gcttc.gcttcrankings.domain.ports.driven.RankingRepository;
import com.gcttc.gcttcrankings.infra.adapters.driven.initialranking.fake.InitialRankingFakeRepository;
import com.gcttc.gcttcrankings.infra.adapters.driven.rankings.fake.RankingFakeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class InitialRankingLoaderTest {

    private InitialRankingLoader instance;
    private RankingRepository rankingRepository;

    @BeforeEach
    public void initService() {
        rankingRepository = new RankingFakeRepository();
        instance = new InitialRankingLoader(new InitialRankingFakeRepository(), rankingRepository);
    }

    @Test
    void whenLoadingValidData_ShouldSaveAllData() {

        instance.load();

        assertEquals(3, rankingRepository.countAllRankings());

    }

}