package com.gcttc.gcttcrankings.domain.services;

import com.gcttc.gcttcrankings.domain.Player;
import com.gcttc.gcttcrankings.domain.Ranking;
import com.gcttc.gcttcrankings.domain.Tournament;
import com.gcttc.gcttcrankings.domain.TournamentRecord;
import com.gcttc.gcttcrankings.domain.ports.driven.RankingRepository;
import com.gcttc.gcttcrankings.domain.ports.driven.TournamentRecordsRepository;
import com.gcttc.gcttcrankings.infra.adapters.driven.rankings.fake.RankingFakeRepository;
import com.gcttc.gcttcrankings.infra.adapters.driven.tournaments.fake.TournamentRecordsFakeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

class TournamentRecordLoaderTest {

    private TournamentRecordLoader instance;
    private TournamentRecordsRepository tournamentRecordsRepository;
    private RankingRepository rankingRepository;

    @BeforeEach
    public void initService() {
        tournamentRecordsRepository = new TournamentRecordsFakeRepository();
        rankingRepository = new RankingFakeRepository();
        instance = new TournamentRecordLoader(tournamentRecordsRepository, rankingRepository);
    }

    @Test
    void whenLoadingValidData_shouldSaveAllData() {

        instance.loadAll();

        assertEquals(2, rankingRepository.countAllTournaments());
        assertEquals(10, rankingRepository.countAllMatches());
        assertEquals(4, rankingRepository.countAllPlayers());

        List<TournamentRecord> tournamentRecords = tournamentRecordsRepository.loadAllTournamentRecords();
        List<Tournament> loadedTournaments = rankingRepository.findAllTournaments();

        assertEquals(tournamentRecords.get(0).getName(), loadedTournaments.get(0).getName());
        assertEquals(tournamentRecords.get(0).getOrganizer(), loadedTournaments.get(0).getOrganizer());
        assertEquals(tournamentRecords.get(0).getDate(), loadedTournaments.get(0).getDate());
        assertEquals(tournamentRecords.get(0).getMatches().size(),
                rankingRepository.findMatchesByTournament(loadedTournaments.get(0).getId()).size());

        assertEquals(tournamentRecords.get(1).getName(), loadedTournaments.get(1).getName());
        assertEquals(tournamentRecords.get(1).getOrganizer(), loadedTournaments.get(1).getOrganizer());
        assertEquals(tournamentRecords.get(1).getDate(), loadedTournaments.get(1).getDate());
        assertEquals(tournamentRecords.get(1).getMatches().size(),
                rankingRepository.findMatchesByTournament(loadedTournaments.get(1).getId()).size());

        assertEquals(6,
                rankingRepository.findRankingsByPlayerId(rankingRepository.findAllPlayers().get(0).getId()).size());

        assertEquals(6,
                rankingRepository.findRankingsByPlayerId(rankingRepository.findAllPlayers().get(1).getId()).size());

        assertEquals(6,
                rankingRepository.findRankingsByPlayerId(rankingRepository.findAllPlayers().get(2).getId()).size());

        assertEquals(6,
                rankingRepository.findRankingsByPlayerId(rankingRepository.findAllPlayers().get(3).getId()).size());

    }

    @Test
    void whenLoadingValidData_shouldUpdateRankingDataForFirstPlayer() {

        instance.loadAll();

        List<TournamentRecord> originalTournaments = tournamentRecordsRepository.loadAllTournamentRecords();

        List<Ranking>
                player1Rankings =
                rankingRepository.findRankingsByPlayerId(rankingRepository.findAllPlayers().get(0).getId());

        assertEquals(originalTournaments.get(0).getDate(), player1Rankings.get(0).getUpdatedAt());
        assertNull(player1Rankings.get(0).getLastMatchID());
        assertNull(player1Rankings.get(0).getPlayerRankingBeforeMatch());
        assertEquals(1000, player1Rankings.get(0).getPlayerRankingAfterMatch());
        assertFalse(player1Rankings.get(0).isCurrentRanking());

        assertEquals(originalTournaments.get(0).getDate(), player1Rankings.get(1).getUpdatedAt());
        assertNotNull(player1Rankings.get(1).getLastMatchID());
        assertEquals(1000, player1Rankings.get(1).getPlayerRankingBeforeMatch());
        assertEquals(1008, player1Rankings.get(1).getPlayerRankingAfterMatch());
        assertFalse(player1Rankings.get(1).isCurrentRanking());

        assertEquals(1016, player1Rankings.get(2).getPlayerRankingAfterMatch());
        assertEquals(1023, player1Rankings.get(3).getPlayerRankingAfterMatch());
        assertEquals(1030, player1Rankings.get(4).getPlayerRankingAfterMatch());

        assertEquals(originalTournaments.get(1).getDate(), player1Rankings.get(5).getUpdatedAt());
        assertNotNull(player1Rankings.get(5).getLastMatchID());
        assertEquals(1030, player1Rankings.get(5).getPlayerRankingBeforeMatch());
        assertEquals(1037, player1Rankings.get(5).getPlayerRankingAfterMatch());
        assertTrue(player1Rankings.get(5).isCurrentRanking());

    }

    @Test
    void whenLoadingValidData_shouldUpdateRankingDataForAnyPlayer() {

        instance.loadAll();

        List<TournamentRecord> originalTournaments = tournamentRecordsRepository.loadAllTournamentRecords();

        List<Ranking>
                player4Rankings =
                rankingRepository.findRankingsByPlayerId(rankingRepository.findAllPlayers().get(3).getId());

        assertEquals(originalTournaments.get(0).getDate(), player4Rankings.get(0).getUpdatedAt());
        assertNull(player4Rankings.get(0).getLastMatchID());
        assertNull(player4Rankings.get(0).getPlayerRankingBeforeMatch());
        assertEquals(1000, player4Rankings.get(0).getPlayerRankingAfterMatch());
        assertFalse(player4Rankings.get(0).isCurrentRanking());

        assertEquals(originalTournaments.get(0).getDate(), player4Rankings.get(1).getUpdatedAt());
        assertNotNull(player4Rankings.get(1).getLastMatchID());
        assertEquals(1000, player4Rankings.get(1).getPlayerRankingBeforeMatch());
        assertEquals(993, player4Rankings.get(1).getPlayerRankingAfterMatch());
        assertFalse(player4Rankings.get(1).isCurrentRanking());

        assertEquals(985, player4Rankings.get(2).getPlayerRankingAfterMatch());
        assertEquals(977, player4Rankings.get(3).getPlayerRankingAfterMatch());
        assertEquals(970, player4Rankings.get(4).getPlayerRankingAfterMatch());

        assertEquals(originalTournaments.get(1).getDate(), player4Rankings.get(5).getUpdatedAt());
        assertNotNull(player4Rankings.get(5).getLastMatchID());
        assertEquals(970, player4Rankings.get(5).getPlayerRankingBeforeMatch());
        assertEquals(963, player4Rankings.get(5).getPlayerRankingAfterMatch());
        assertTrue(player4Rankings.get(5).isCurrentRanking());

    }

    @Test
    void whenLoadingValidDataAndAPlayerAlreadyExist_shouldExecuteWithoutErrors() {

        rankingRepository.savePlayer(new Player("Name1", "Lastname1"));

        instance.loadAll();

        assertEquals(2, rankingRepository.countAllTournaments());
        assertEquals(10, rankingRepository.countAllMatches());
        assertEquals(4, rankingRepository.countAllPlayers());

    }

}