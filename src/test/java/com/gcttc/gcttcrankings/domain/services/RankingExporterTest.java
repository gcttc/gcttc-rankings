package com.gcttc.gcttcrankings.domain.services;

import com.gcttc.gcttcrankings.domain.Player;
import com.gcttc.gcttcrankings.domain.Ranking;
import com.gcttc.gcttcrankings.domain.ports.driven.RankingRepository;
import com.gcttc.gcttcrankings.infra.adapters.driven.exportedrankings.fake.ExportedRankingsFakeRepository;
import com.gcttc.gcttcrankings.infra.adapters.driven.rankings.fake.RankingFakeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.Clock;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RankingExporterTest {

    private RankingExporter instance;
    private RankingRepository rankingRepository;
    private ExportedRankingsFakeRepository exportedRankingsRepository;

    @BeforeEach
    public void initService() {
        rankingRepository = new RankingFakeRepository();
        exportedRankingsRepository = new ExportedRankingsFakeRepository();
        instance = new RankingExporter(rankingRepository, exportedRankingsRepository);
    }

    @Test
    void whenExportingValidData_ShouldExportAllData() {

        LocalDate anyDate = LocalDate.now(Clock.systemDefaultZone());

        List<Ranking> givenRankings = new ArrayList<>();
        givenRankings.add(Ranking.buildFirstRankingFor(anyDate, new Player("Any name", "Any lastname"), 1400));
        givenRankings.add(Ranking.buildFirstRankingFor(anyDate, new Player("Any name 2", "Any lastname 2"), 1200));
        givenRankings.add(Ranking.buildFirstRankingFor(anyDate, new Player("Any name 3", "Any lastname 3"), 1800));

        rankingRepository.saveRankings(givenRankings);

        instance.exportAll();

        assertEquals(3, exportedRankingsRepository.getRankingReport().getRankings().size());

    }

}