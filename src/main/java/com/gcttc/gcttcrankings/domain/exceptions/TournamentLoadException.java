package com.gcttc.gcttcrankings.domain.exceptions;

public class TournamentLoadException
        extends RuntimeException {

    public TournamentLoadException(Throwable cause) {
        super("Unexpected error when loading the tournaments.", cause);
    }

}
