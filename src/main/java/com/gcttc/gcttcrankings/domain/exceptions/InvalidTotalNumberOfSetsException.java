package com.gcttc.gcttcrankings.domain.exceptions;

public class InvalidTotalNumberOfSetsException
        extends RuntimeException {

    public InvalidTotalNumberOfSetsException(String message) {
        super(message);
    }

}
