package com.gcttc.gcttcrankings.domain.exceptions;

public class InitialRankingLoadException
        extends RuntimeException {

    public InitialRankingLoadException(Throwable cause) {
        super("Unexpected error when loading the initial ranking.", cause);
    }

    public InitialRankingLoadException(String cause) {
        super("The initial ranking file does not exist.");
    }

}
