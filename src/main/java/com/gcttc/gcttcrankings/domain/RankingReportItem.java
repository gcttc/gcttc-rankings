package com.gcttc.gcttcrankings.domain;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDate;
import java.util.UUID;

public class RankingReportItem {

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private LocalDate updatedAt;
    private LastMatchReportItem lastMatch;
    private UUID playerId;
    private String playerName;
    private String playerLastname;
    private Integer playerRankingBeforeMatch;
    private Integer playerRankingAfterMatch;
    private boolean currentRanking;

    public RankingReportItem() {
    }

    public RankingReportItem(LocalDate updatedAt,
                             LastMatchReportItem lastMatch,
                             UUID playerId,
                             String playerName,
                             String playerLastname,
                             Integer playerRankingBeforeMatch,
                             Integer playerRankingAfterMatch,
                             boolean currentRanking) {
        this.updatedAt = updatedAt;
        this.lastMatch = lastMatch;
        this.playerId = playerId;
        this.playerName = playerName;
        this.playerLastname = playerLastname;
        this.playerRankingBeforeMatch = playerRankingBeforeMatch;
        this.playerRankingAfterMatch = playerRankingAfterMatch;
        this.currentRanking = currentRanking;
    }

    public LocalDate getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDate updatedAt) {
        this.updatedAt = updatedAt;
    }

    public LastMatchReportItem getLastMatch() {
        return lastMatch;
    }

    public void setLastMatch(LastMatchReportItem lastMatch) {
        this.lastMatch = lastMatch;
    }

    public UUID getPlayerId() {
        return playerId;
    }

    public void setPlayerId(UUID playerId) {
        this.playerId = playerId;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public String getPlayerLastname() {
        return playerLastname;
    }

    public void setPlayerLastname(String playerLastname) {
        this.playerLastname = playerLastname;
    }

    public Integer getPlayerRankingBeforeMatch() {
        return playerRankingBeforeMatch;
    }

    public void setPlayerRankingBeforeMatch(Integer playerRankingBeforeMatch) {
        this.playerRankingBeforeMatch = playerRankingBeforeMatch;
    }

    public Integer getPlayerRankingAfterMatch() {
        return playerRankingAfterMatch;
    }

    public void setPlayerRankingAfterMatch(Integer playerRankingAfterMatch) {
        this.playerRankingAfterMatch = playerRankingAfterMatch;
    }

    public boolean isCurrentRanking() {
        return currentRanking;
    }

    public void setCurrentRanking(boolean currentRanking) {
        this.currentRanking = currentRanking;
    }
}
