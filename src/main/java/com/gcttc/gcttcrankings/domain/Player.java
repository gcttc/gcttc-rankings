package com.gcttc.gcttcrankings.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gcttc.gcttcrankings.domain.exceptions.InvalidNameException;
import org.springframework.util.StringUtils;

import java.util.UUID;

public class Player {

    private UUID id;
    private String name;
    private String lastname;

    public Player() {
    }

    public Player(String name, String lastname) {
        this(UUID.randomUUID(), name, lastname);
    }

    public Player(UUID id, String name, String lastname) {
        this.id = id;

        if (!StringUtils.hasText(name) || !StringUtils.hasText(lastname)) {
            throw new InvalidNameException();
        }

        this.name = name;
        this.lastname = lastname;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    @JsonIgnore
    public String getFullName() {
        return lastname + " " + name;
    }

}
