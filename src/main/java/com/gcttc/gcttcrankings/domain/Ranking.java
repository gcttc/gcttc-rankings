package com.gcttc.gcttcrankings.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.gcttc.gcttcrankings.domain.exceptions.InvalidRankingException;

import java.time.LocalDate;
import java.util.UUID;

public class Ranking {

    public static final int DEFAULT_FIRST_RANKING_VALUE = 1000;
    private UUID id;
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private LocalDate updatedAt;
    private UUID lastMatchID;
    private Player player;
    private Integer playerRankingBeforeMatch;
    private Integer playerRankingAfterMatch;
    private boolean currentRanking;


    public Ranking() {
    }

    private Ranking(LocalDate updatedAt, Player player, String playerRankingAfterMatchAsString) {
        this.id = UUID.randomUUID();
        this.updatedAt = updatedAt;
        this.player = player;
        try {
            this.playerRankingAfterMatch = Integer.valueOf(playerRankingAfterMatchAsString);
        } catch (NumberFormatException nfe) {
            throw new InvalidRankingException();
        }
        this.currentRanking = true;
    }

    private Ranking(LocalDate updatedAt, Player player, Integer playerRankingAfterMatch) {
        this.id = UUID.randomUUID();
        this.updatedAt = updatedAt;
        this.player = player;
        this.playerRankingAfterMatch = playerRankingAfterMatch;
        this.currentRanking = true;
    }

    private Ranking(LocalDate updatedAt,
                    UUID lastMatchID,
                    Player player,
                    Integer playerRankingBeforeMatch,
                    Integer playerRankingAfterMatch) {
        this.id = UUID.randomUUID();
        this.updatedAt = updatedAt;
        this.lastMatchID = lastMatchID;
        this.player = player;
        this.playerRankingBeforeMatch = playerRankingBeforeMatch;
        this.playerRankingAfterMatch = playerRankingAfterMatch;
        this.currentRanking = true;
    }

    private Ranking(UUID id,
                    LocalDate updatedAt,
                    UUID lastMatchID,
                    Player player,
                    Integer playerRankingBeforeMatch,
                    Integer playerRankingAfterMatch,
                    boolean currentRanking) {
        this.id = id;
        this.updatedAt = updatedAt;
        this.lastMatchID = lastMatchID;
        this.player = player;
        this.playerRankingBeforeMatch = playerRankingBeforeMatch;
        this.playerRankingAfterMatch = playerRankingAfterMatch;
        this.currentRanking = currentRanking;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public LocalDate getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDate updatedAt) {
        this.updatedAt = updatedAt;
    }

    public UUID getLastMatchID() {
        return lastMatchID;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Integer getPlayerRankingBeforeMatch() {
        return playerRankingBeforeMatch;
    }

    public Integer getPlayerRankingAfterMatch() {
        return playerRankingAfterMatch;
    }

    public boolean isCurrentRanking() {
        return currentRanking;
    }

    public void setCurrentRanking(boolean currentRanking) {
        this.currentRanking = currentRanking;
    }

    public static Ranking buildFirstRankingFor(LocalDate updatedAt, Player player, String playerRankingAfterMatch) {
        return new Ranking(updatedAt, player, playerRankingAfterMatch);
    }

    public static Ranking buildFirstDefaultRankingFor(LocalDate updatedAt, Player player) {
        return new Ranking(updatedAt, player, DEFAULT_FIRST_RANKING_VALUE);
    }

    public static Ranking buildFirstRankingFor(LocalDate updatedAt, Player player, Integer playerRankingAfterMatch) {
        return new Ranking(updatedAt, player, playerRankingAfterMatch);
    }

    public static Ranking buildAdditionalRankingFor(LocalDate updatedAt,
                                                    UUID lastMatchID,
                                                    Player player,
                                                    Integer playerRankingBeforeMatch,
                                                    Integer playerRankingAfterMatch) {
        return new Ranking(updatedAt, lastMatchID, player, playerRankingBeforeMatch, playerRankingAfterMatch);
    }

    public static Ranking buildRankingAs(UUID id,
                                         LocalDate updatedAt,
                                         UUID lastMatchID,
                                         Player player,
                                         Integer playerRankingBeforeMatch,
                                         Integer playerRankingAfterMatch,
                                         boolean currentRanking) {
        return new Ranking(id,
                updatedAt,
                lastMatchID,
                player,
                playerRankingBeforeMatch,
                playerRankingAfterMatch,
                currentRanking);

    }

}
