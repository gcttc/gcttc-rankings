package com.gcttc.gcttcrankings.domain.services;

import com.gcttc.gcttcrankings.domain.ports.driven.InitialRankingRepository;
import com.gcttc.gcttcrankings.domain.ports.driven.RankingRepository;
import com.gcttc.gcttcrankings.domain.ports.driver.ForLoadingInitialRanking;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InitialRankingLoader
        implements ForLoadingInitialRanking {

    private final InitialRankingRepository initialRankingRepository;
    private final RankingRepository rankingRepository;

    private static final Logger LOGGER = LoggerFactory.getLogger(InitialRankingLoader.class);

    public InitialRankingLoader(InitialRankingRepository initialRankingRepository,
                                RankingRepository rankingRepository) {
        this.initialRankingRepository = initialRankingRepository;
        this.rankingRepository = rankingRepository;
    }

    @Override
    public void load() {

        this.rankingRepository.saveRankings(this.initialRankingRepository.loadAllRankings());

        LOGGER.info("Total of Initial Ranking records saved: {}", this.rankingRepository.countAllRankings());

    }

}
