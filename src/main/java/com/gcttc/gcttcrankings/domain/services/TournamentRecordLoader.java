package com.gcttc.gcttcrankings.domain.services;

import com.gcttc.gcttcrankings.domain.Match;
import com.gcttc.gcttcrankings.domain.MatchRecord;
import com.gcttc.gcttcrankings.domain.Player;
import com.gcttc.gcttcrankings.domain.Ranking;
import com.gcttc.gcttcrankings.domain.Tournament;
import com.gcttc.gcttcrankings.domain.TournamentRecord;
import com.gcttc.gcttcrankings.domain.ports.driven.RankingRepository;
import com.gcttc.gcttcrankings.domain.ports.driven.TournamentRecordsRepository;
import com.gcttc.gcttcrankings.domain.ports.driver.ForLoadingTournaments;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

public class TournamentRecordLoader
        implements ForLoadingTournaments {

    private final TournamentRecordsRepository tournamentRecordsRepository;
    private final RankingRepository rankingRepository;

    private final USATTRankingCalculator rankingCalculator = new USATTRankingCalculator();

    private static final Logger log = LoggerFactory.getLogger(TournamentRecordLoader.class);

    public TournamentRecordLoader(TournamentRecordsRepository tournamentRecordsRepository,
                                  RankingRepository rankingRepository) {
        this.tournamentRecordsRepository = tournamentRecordsRepository;
        this.rankingRepository = rankingRepository;
    }

    @Override
    public void loadAll() {

        List<TournamentRecord> tournamentRecords = this.tournamentRecordsRepository.loadAllTournamentRecords();

        for (TournamentRecord tournamentRecord : tournamentRecords) {

            Tournament
                    tournament =
                    new Tournament(tournamentRecord.getName(),
                            tournamentRecord.getOrganizer(),
                            tournamentRecord.getDate());
            this.rankingRepository.saveTournament(tournament);

            for (MatchRecord matchRecord : tournamentRecord.getMatches()) {

                Player
                        playerA =
                        this.rankingRepository.findPlayerByNameAndLastname(matchRecord.getPlayerA().getName(),
                                matchRecord.getPlayerA().getLastname());
                if (playerA == null) {
                    playerA = new Player(matchRecord.getPlayerA().getName(), matchRecord.getPlayerA().getLastname());
                    this.rankingRepository.savePlayer(playerA);
                }

                Player
                        playerB =
                        this.rankingRepository.findPlayerByNameAndLastname(matchRecord.getPlayerB().getName(),
                                matchRecord.getPlayerB().getLastname());
                if (playerB == null) {
                    playerB = new Player(matchRecord.getPlayerB().getName(), matchRecord.getPlayerB().getLastname());
                    this.rankingRepository.savePlayer(playerB);
                }

                Match
                        hydratedMatch =
                        new Match(tournament,
                                playerA,
                                matchRecord.getPlayerAWonSets(),
                                playerB,
                                matchRecord.getPlayerBWonSets());

                this.rankingRepository.saveMatch(hydratedMatch);

                log.debug("Match: {} <Sets: {}> vs {} <Sets: {}>",
                        playerA.getFullName(),
                        matchRecord.getPlayerAWonSets(),
                        playerB.getFullName(),
                        matchRecord.getPlayerBWonSets());

                registerNewRankingsAfterMatch(hydratedMatch.getId(),
                        tournamentRecord.getDate(),
                        hydratedMatch.getWinner(),
                        hydratedMatch.getLoser());

            }

        }

    }

    private void registerNewRankingsAfterMatch(UUID matchId,
                                               LocalDate rankingDate,
                                               Player winnerPlayer,
                                               Player loserPlayer) {

        //Tries to find last ranking for winner
        Ranking winnerLastRanking = this.rankingRepository.findLastRankingByPlayerId(winnerPlayer.getId());
        if (winnerLastRanking == null) {
            //If there is no last ranking, creates a new one with default value
            winnerLastRanking = Ranking.buildFirstDefaultRankingFor(rankingDate, winnerPlayer);
            this.rankingRepository.saveRanking(winnerLastRanking);
        }
        //Updates the last ranking with the "not current" state
        winnerLastRanking.setCurrentRanking(false);
        this.rankingRepository.saveRanking(winnerLastRanking);

        //Tries to find last ranking for looer
        Ranking loserLastRanking = this.rankingRepository.findLastRankingByPlayerId(loserPlayer.getId());
        if (loserLastRanking == null) {
            //If there is no last ranking, creates a new one with default value
            loserLastRanking = Ranking.buildFirstDefaultRankingFor(rankingDate, loserPlayer);
            this.rankingRepository.saveRanking(loserLastRanking);
        }
        //Updates the last ranking with the "not current" state
        loserLastRanking.setCurrentRanking(false);
        this.rankingRepository.saveRanking(loserLastRanking);

        Integer
                winnerNewRankingValue =
                rankingCalculator.calculateWinnerNewRanking(winnerLastRanking.getPlayerRankingAfterMatch(),
                        loserLastRanking.getPlayerRankingAfterMatch());
        this.rankingRepository.saveRanking(Ranking.buildAdditionalRankingFor(rankingDate,
                matchId,
                winnerPlayer,
                winnerLastRanking.getPlayerRankingAfterMatch(),
                winnerNewRankingValue));

        Integer
                loserNewRankingValue =
                rankingCalculator.calculateLoserNewRanking(winnerLastRanking.getPlayerRankingAfterMatch(),
                        loserLastRanking.getPlayerRankingAfterMatch());

        this.rankingRepository.saveRanking(Ranking.buildAdditionalRankingFor(rankingDate,
                matchId,
                loserPlayer,
                loserLastRanking.getPlayerRankingAfterMatch(),
                loserNewRankingValue));

        log.info("Match result -- Winner: {} <{} -> {}> -- Loser: {} <{} -> {}>",
                winnerPlayer.getFullName(),
                winnerLastRanking.getPlayerRankingAfterMatch(),
                winnerNewRankingValue,
                loserPlayer.getFullName(),
                loserLastRanking.getPlayerRankingAfterMatch(),
                loserNewRankingValue);

    }

}
