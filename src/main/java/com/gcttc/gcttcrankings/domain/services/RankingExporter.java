package com.gcttc.gcttcrankings.domain.services;

import com.gcttc.gcttcrankings.domain.LastMatchReportItem;
import com.gcttc.gcttcrankings.domain.Match;
import com.gcttc.gcttcrankings.domain.Ranking;
import com.gcttc.gcttcrankings.domain.RankingReport;
import com.gcttc.gcttcrankings.domain.RankingReportItem;
import com.gcttc.gcttcrankings.domain.ports.driven.ExportedRankingsRepository;
import com.gcttc.gcttcrankings.domain.ports.driven.RankingRepository;
import com.gcttc.gcttcrankings.domain.ports.driver.ForExportingRankings;

import java.time.Clock;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class RankingExporter
        implements ForExportingRankings {

    private final RankingRepository rankingRepository;
    private final ExportedRankingsRepository exportedRankingsRepository;

    public RankingExporter(RankingRepository rankingRepository, ExportedRankingsRepository exportedRankingsRepository) {
        this.rankingRepository = rankingRepository;
        this.exportedRankingsRepository = exportedRankingsRepository;
    }

    @Override
    public void exportAll() {

        List<RankingReportItem> rankings = new ArrayList<>();

        List<Ranking> allRankings = rankingRepository.findAllRankings();

        for (Ranking ranking : allRankings) {

            LastMatchReportItem lastMatchReport = null;

            if (ranking.getLastMatchID() != null) {
                Match match = rankingRepository.findMatchById(ranking.getLastMatchID());
                Ranking pairRanking = rankingRepository.findPairRankingByRanking(ranking);
                lastMatchReport =
                        new LastMatchReportItem(match.getTournament().getName(),
                                match.getOpponentFullName(ranking.getPlayer().getId()),
                                match.getOpponentWonSets(ranking.getPlayer().getId()),
                                pairRanking.getPlayerRankingBeforeMatch(),
                                pairRanking.getPlayerRankingAfterMatch(),
                                match.getPlayerWonSets(ranking.getPlayer().getId()));
            }

            rankings.add(new RankingReportItem(ranking.getUpdatedAt(),
                    lastMatchReport,
                    ranking.getPlayer().getId(),
                    ranking.getPlayer().getName(),
                    ranking.getPlayer().getLastname(),
                    ranking.getPlayerRankingBeforeMatch(),
                    ranking.getPlayerRankingAfterMatch(),
                    ranking.isCurrentRanking()));
        }

        RankingReport report = new RankingReport(LocalDateTime.now(Clock.systemDefaultZone()), rankings);

        exportedRankingsRepository.save(report);

    }

}
