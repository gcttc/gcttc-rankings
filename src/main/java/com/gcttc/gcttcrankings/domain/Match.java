package com.gcttc.gcttcrankings.domain;

import com.gcttc.gcttcrankings.domain.exceptions.InvalidTotalNumberOfSetsException;
import com.gcttc.gcttcrankings.domain.exceptions.SameWinningSetsException;

import java.util.UUID;

public class Match {

    private final UUID id;
    private Tournament tournament;
    private final Player playerA;
    private final Short playerAWonSets;
    private final Player playerB;
    private final Short playerBWonSets;

    public Match(Tournament tournament, Player playerA, Short playerAWonSets, Player playerB, Short playerBWonSets) {
        this(UUID.randomUUID(), tournament, playerA, playerAWonSets, playerB, playerBWonSets);
    }

    public Match(UUID id,
                 Tournament tournament,
                 Player playerA,
                 Short playerAWonSets,
                 Player playerB,
                 Short playerBWonSets) {

        if (playerAWonSets.equals(playerBWonSets)) {
            throw new SameWinningSetsException();
        }
        int playedSets = playerAWonSets + playerBWonSets;
        if (playedSets < 2 || playedSets > 7) {
            throw new InvalidTotalNumberOfSetsException("Player A: " + playerA + " - " + playerAWonSets + " sets\n" +
                    "Player B: " + playerB + " - " + playerBWonSets + " sets");
        }

        this.id = id;
        this.tournament = tournament;
        this.playerA = playerA;
        this.playerAWonSets = playerAWonSets;
        this.playerB = playerB;
        this.playerBWonSets = playerBWonSets;
    }

    public UUID getId() {
        return id;
    }

    public Tournament getTournament() {
        return tournament;
    }

    public void setTournament(Tournament tournament) {
        this.tournament = tournament;
    }

    public Player getPlayerA() {
        return playerA;
    }

    public Short getPlayerAWonSets() {
        return playerAWonSets;
    }

    public Player getPlayerB() {
        return playerB;
    }

    public Short getPlayerBWonSets() {
        return playerBWonSets;
    }

    public Player getWinner() {
        if (playerAWonSets > playerBWonSets) {
            return playerA;
        } else {
            return playerB;
        }
    }

    public Player getLoser() {
        if (playerAWonSets > playerBWonSets) {
            return playerB;
        } else {
            return playerA;
        }
    }

    public Player getOpponent(UUID playerId) {
        if (playerA.getId().equals(playerId)) {
            return playerB;
        } else if (playerB.getId().equals(playerId)) {
            return playerA;
        } else {
            return null;
        }
    }

    public String getOpponentFullName(UUID playerId) {
        return getOpponent(playerId).getFullName();
    }

    public Short getOpponentWonSets(UUID playerId) {
        if (playerA.getId().equals(playerId)) {
            return playerBWonSets;
        } else if (playerB.getId().equals(playerId)) {
            return playerAWonSets;
        } else {
            return null;
        }
    }

    public Short getPlayerWonSets(UUID playerId) {
        if (playerA.getId().equals(playerId)) {
            return playerAWonSets;
        } else if (playerB.getId().equals(playerId)) {
            return playerBWonSets;
        } else {
            return null;
        }
    }

}
