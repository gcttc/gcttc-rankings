package com.gcttc.gcttcrankings.domain;

public class LastMatchReportItem {

    private String tournamentName;
    private String opponentName;
    private Short opponentWonSets;
    private Integer opponentRankingBeforeMatch;
    private Integer opponentRankingAfterMatch;
    private Short playerWonSets;

    public LastMatchReportItem() {
    }

    public LastMatchReportItem(String tournamentName,
                               String opponentName,
                               Short opponentWonSets,
                               Integer opponentRankingBeforeMatch,
                               Integer opponentRankingAfterMatch,
                               Short playerWonSets) {
        this.tournamentName = tournamentName;
        this.opponentName = opponentName;
        this.opponentWonSets = opponentWonSets;
        this.opponentRankingBeforeMatch = opponentRankingBeforeMatch;
        this.opponentRankingAfterMatch = opponentRankingAfterMatch;
        this.playerWonSets = playerWonSets;
    }

    public String getTournamentName() {
        return tournamentName;
    }

    public void setTournamentName(String tournamentName) {
        this.tournamentName = tournamentName;
    }

    public String getOpponentName() {
        return opponentName;
    }

    public void setOpponentName(String opponentName) {
        this.opponentName = opponentName;
    }

    public Short getOpponentWonSets() {
        return opponentWonSets;
    }

    public void setOpponentWonSets(Short opponentWonSets) {
        this.opponentWonSets = opponentWonSets;
    }

    public Integer getOpponentRankingBeforeMatch() {
        return opponentRankingBeforeMatch;
    }

    public void setOpponentRankingBeforeMatch(Integer opponentRankingBeforeMatch) {
        this.opponentRankingBeforeMatch = opponentRankingBeforeMatch;
    }

    public Integer getOpponentRankingAfterMatch() {
        return opponentRankingAfterMatch;
    }

    public void setOpponentRankingAfterMatch(Integer opponentRankingAfterMatch) {
        this.opponentRankingAfterMatch = opponentRankingAfterMatch;
    }

    public Short getPlayerWonSets() {
        return playerWonSets;
    }

    public void setPlayerWonSets(Short playerWonSets) {
        this.playerWonSets = playerWonSets;
    }

}
