package com.gcttc.gcttcrankings.domain;

import java.time.LocalDate;
import java.util.UUID;

public class Tournament {

    private UUID id;
    private String name;
    private String organizer;
    private LocalDate date;

    public Tournament(String name, String organizer, LocalDate date) {
        this(UUID.randomUUID(), name, organizer, date);
    }

    public Tournament(UUID id, String name, String organizer, LocalDate date) {
        this.id = id;
        this.name = name;
        this.organizer = organizer;
        this.date = date;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrganizer() {
        return organizer;
    }

    public void setOrganizer(String organizer) {
        this.organizer = organizer;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

}
