package com.gcttc.gcttcrankings.domain;

import com.gcttc.gcttcrankings.domain.exceptions.InvalidTotalNumberOfSetsException;
import com.gcttc.gcttcrankings.domain.exceptions.SameWinningSetsException;

public class MatchRecord {

    private final PlayerRecord playerA;
    private final Short playerAWonSets;
    private final PlayerRecord playerB;
    private final Short playerBWonSets;

    public MatchRecord(PlayerRecord playerA,
                       Short playerAWonSets,
                       PlayerRecord playerB,
                       Short playerBWonSets) {

        if (playerAWonSets.equals(playerBWonSets)) {
            throw new SameWinningSetsException();
        }
        int playedSets = playerAWonSets + playerBWonSets;
        if (playedSets < 2 || playedSets > 7) {
            throw new InvalidTotalNumberOfSetsException("Player A: " + playerA + " - " + playerAWonSets + " sets\n" +
                    "Player B: " + playerB + " - " + playerBWonSets + " sets");
        }

        this.playerA = playerA;
        this.playerAWonSets = playerAWonSets;
        this.playerB = playerB;
        this.playerBWonSets = playerBWonSets;
    }

    public PlayerRecord getPlayerA() {
        return playerA;
    }

    public Short getPlayerAWonSets() {
        return playerAWonSets;
    }

    public PlayerRecord getPlayerB() {
        return playerB;
    }

    public Short getPlayerBWonSets() {
        return playerBWonSets;
    }

    public PlayerRecord getWinner() {
        if (playerAWonSets > playerBWonSets) {
            return playerA;
        } else {
            return playerB;
        }
    }

    public PlayerRecord getLoser() {
        if (playerAWonSets > playerBWonSets) {
            return playerB;
        } else {
            return playerA;
        }
    }

}
