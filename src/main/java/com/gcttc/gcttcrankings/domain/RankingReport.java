package com.gcttc.gcttcrankings.domain;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDateTime;
import java.util.List;

public class RankingReport {

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private LocalDateTime date;
    private List<RankingReportItem> rankings;

    public RankingReport() {
    }

    public RankingReport(LocalDateTime date, List<RankingReportItem> rankings) {
        this.date = date;
        this.rankings = rankings;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public List<RankingReportItem> getRankings() {
        return rankings;
    }

    public void setRankings(List<RankingReportItem> rankings) {
        this.rankings = rankings;
    }
}
