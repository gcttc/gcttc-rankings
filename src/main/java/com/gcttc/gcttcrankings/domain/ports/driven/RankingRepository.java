package com.gcttc.gcttcrankings.domain.ports.driven;

import com.gcttc.gcttcrankings.domain.Match;
import com.gcttc.gcttcrankings.domain.Player;
import com.gcttc.gcttcrankings.domain.Ranking;
import com.gcttc.gcttcrankings.domain.Tournament;

import java.util.List;
import java.util.UUID;

public interface RankingRepository {

    void saveRankings(List<Ranking> rankingEntries);

    void saveRanking(Ranking ranking);

    List<Ranking> findAllRankings();

    Long countAllRankings();

    Ranking findLastRankingByPlayerId(UUID playerID);

    List<Ranking> findRankingsByPlayerId(UUID playerID);

    Ranking findPairRankingByRanking(Ranking ranking);

    void saveTournament(Tournament tournament);

    List<Tournament> findAllTournaments();

    Long countAllTournaments();

    void saveMatch(Match match);

    Long countAllMatches();

    List<Match> findMatchesByTournament(UUID tournamentID);

    Match findMatchById(UUID matchID);

    void savePlayer(Player player);

    Long countAllPlayers();

    List<Player> findAllPlayers();

    Player findPlayerByNameAndLastname(String name, String lastname);

}
