package com.gcttc.gcttcrankings.domain.ports.driver;

public interface ForLoadingInitialRanking {

    void load();

}
