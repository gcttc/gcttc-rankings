package com.gcttc.gcttcrankings.domain.ports.driver;

public interface ForExportingRankings {

    void exportAll();

}
