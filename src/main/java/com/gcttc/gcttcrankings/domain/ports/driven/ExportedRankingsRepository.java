package com.gcttc.gcttcrankings.domain.ports.driven;

import com.gcttc.gcttcrankings.domain.RankingReport;

public interface ExportedRankingsRepository {

    void save(RankingReport rankingReport);

}
