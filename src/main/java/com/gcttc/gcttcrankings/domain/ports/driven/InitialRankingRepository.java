package com.gcttc.gcttcrankings.domain.ports.driven;

import com.gcttc.gcttcrankings.domain.Ranking;

import java.util.List;

public interface InitialRankingRepository {

    List<Ranking> loadAllRankings();

}
