package com.gcttc.gcttcrankings.domain.ports.driven;

import com.gcttc.gcttcrankings.domain.TournamentRecord;

import java.util.List;

public interface TournamentRecordsRepository {

    List<TournamentRecord> loadAllTournamentRecords();

}
