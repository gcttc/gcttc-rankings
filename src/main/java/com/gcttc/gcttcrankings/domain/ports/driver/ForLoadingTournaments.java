package com.gcttc.gcttcrankings.domain.ports.driver;

public interface ForLoadingTournaments {

    void loadAll();

}
