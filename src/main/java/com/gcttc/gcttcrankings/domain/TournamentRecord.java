package com.gcttc.gcttcrankings.domain;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class TournamentRecord {

    private String name;
    private String organizer;
    private LocalDate date;
    private List<MatchRecord> matches;

    public TournamentRecord(String name, String organizer, LocalDate date, List<MatchRecord> matches) {
        this.name = name;
        this.organizer = organizer;
        this.date = date;
        this.matches = matches;
    }

    public TournamentRecord(String name, String organizer, LocalDate date) {
        this(name, organizer, date, new ArrayList<>());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrganizer() {
        return organizer;
    }

    public void setOrganizer(String organizer) {
        this.organizer = organizer;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public List<MatchRecord> getMatches() {
        return matches;
    }

    public void setMatches(List<MatchRecord> matches) {
        this.matches = matches;
    }

    public void registerMatch(PlayerRecord playerA, Short playerAWonSets, PlayerRecord playerB, Short playerBWonSets) {
        this.matches.add(new MatchRecord(playerA, playerAWonSets, playerB, playerBWonSets));
    }

}
