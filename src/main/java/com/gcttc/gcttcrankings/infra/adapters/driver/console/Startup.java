package com.gcttc.gcttcrankings.infra.adapters.driver.console;

import com.gcttc.gcttcrankings.domain.ports.driver.ForExportingRankings;
import com.gcttc.gcttcrankings.domain.ports.driver.ForLoadingInitialRanking;
import com.gcttc.gcttcrankings.domain.ports.driver.ForLoadingTournaments;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class Startup
        implements CommandLineRunner {

    private final ForLoadingInitialRanking forLoadingInitialRanking;
    private final ForLoadingTournaments forLoadingTournaments;
    private final ForExportingRankings forExportingRankings;

    private static final Logger LOGGER = LoggerFactory.getLogger(Startup.class);

    public Startup(ForLoadingInitialRanking forLoadingInitialRanking,
                   ForLoadingTournaments forLoadingTournaments,
                   ForExportingRankings forExportingRankings) {
        this.forLoadingInitialRanking = forLoadingInitialRanking;
        this.forLoadingTournaments = forLoadingTournaments;
        this.forExportingRankings = forExportingRankings;
    }

    @Override
    public void run(String... args) {

        LOGGER.info("Loading initial ranking...");
        forLoadingInitialRanking.load();
        LOGGER.info("Loading tournaments...");
        forLoadingTournaments.loadAll();
        LOGGER.info("Exporting ranking...");
        forExportingRankings.exportAll();

    }

}
