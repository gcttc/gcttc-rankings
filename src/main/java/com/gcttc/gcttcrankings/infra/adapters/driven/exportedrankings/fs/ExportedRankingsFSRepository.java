package com.gcttc.gcttcrankings.infra.adapters.driven.exportedrankings.fs;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.gcttc.gcttcrankings.domain.RankingReport;
import com.gcttc.gcttcrankings.domain.exceptions.ExportRankingException;
import com.gcttc.gcttcrankings.domain.ports.driven.ExportedRankingsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@Component
public class ExportedRankingsFSRepository
        implements ExportedRankingsRepository {

    private final String outputDirectory;
    private final ObjectMapper mapper;

    private static final Logger LOG = LoggerFactory.getLogger(ExportedRankingsFSRepository.class);

    public ExportedRankingsFSRepository(@Value("${process.output-directory:data/output}") String outputDirectory) {
        this.outputDirectory = outputDirectory;
        mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
    }

    @Override
    public void save(RankingReport rankingReport) {

        try {
            Path path = Path.of(outputDirectory);
            Files.createDirectories(path);
            path = path.resolve("rankings.json");
            mapper.writeValue(path.toFile(), rankingReport);
        } catch (IOException e) {
            LOG.error("Unexpected error: ", e);
            throw new ExportRankingException();
        }

    }

}
