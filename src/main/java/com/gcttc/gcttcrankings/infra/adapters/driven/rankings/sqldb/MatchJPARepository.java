package com.gcttc.gcttcrankings.infra.adapters.driven.rankings.sqldb;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface MatchJPARepository
        extends CrudRepository<MatchJPAEntity, UUID> {

    List<MatchJPAEntity> findByTournamentId(UUID tournamentID);

}
