package com.gcttc.gcttcrankings.infra.adapters.driven.rankings.sqldb;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface RankingJPARepository
        extends CrudRepository<RankingJPAEntity, UUID> {

    List<RankingJPAEntity> findAll();

    List<RankingJPAEntity> findByPlayerId(UUID playerID);

    List<RankingJPAEntity> findByLastMatchID(UUID lastMatchID);

}
