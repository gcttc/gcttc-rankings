package com.gcttc.gcttcrankings.infra.adapters.driven.initialranking.fs;

import com.gcttc.gcttcrankings.domain.Player;
import com.gcttc.gcttcrankings.domain.Ranking;
import com.gcttc.gcttcrankings.domain.exceptions.InitialRankingLoadException;
import com.gcttc.gcttcrankings.domain.exceptions.InvalidNameException;
import com.gcttc.gcttcrankings.domain.exceptions.InvalidRankingException;
import com.gcttc.gcttcrankings.domain.ports.driven.InitialRankingRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class InitialRankingFSRepository
        implements InitialRankingRepository {

    private final String initialRankingFile;

    private static final String DELIMITER = ",";
    private static final Logger LOGGER = LoggerFactory.getLogger(InitialRankingFSRepository.class);

    public InitialRankingFSRepository(@Value("${process.initial-ranking-file:data/initial-ranking.csv}") String initialRankingFile) {
        this.initialRankingFile = initialRankingFile;
    }

    @Override
    public List<Ranking> loadAllRankings() {

        List<Ranking> loadedRankings = new ArrayList<>();

        LOGGER.info("Starting Initial Ranking Load process from file <{}>...", initialRankingFile);

        if (!Paths.get(initialRankingFile).toFile().exists()) {
            throw new InitialRankingLoadException("");
        }

        LocalDate rankingDate = getRankingDateFromFileName(Paths.get(initialRankingFile).getFileName().toString());

        try (BufferedReader br = new BufferedReader(new FileReader(initialRankingFile))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(DELIMITER);
                if (values.length < 4) continue;
                addNewRankingFromValues(loadedRankings, values, rankingDate);
            }
        } catch (IOException e) {
            throw new InitialRankingLoadException(e);
        }

        LOGGER.info("Finishing Initial Ranking Load process. Total of records read: {}", loadedRankings.size());

        return loadedRankings;

    }

    private static LocalDate getRankingDateFromFileName(String filename) {
        filename = filename.replace(".csv", "");
        return LocalDate.parse(filename.split("-")[filename.split("-").length - 1],
                DateTimeFormatter.ofPattern("yyyyMMdd"));
    }

    private static void addNewRankingFromValues(List<Ranking> loadedRankings, String[] values, LocalDate rankingDate) {
        try {
            loadedRankings.add(Ranking.buildFirstRankingFor(rankingDate,
                    new Player(values[2].trim(), values[1].trim()),
                    values[3].trim()));
        } catch (InvalidNameException | InvalidRankingException e) {
            LOGGER.warn("Player record {} not valid", Arrays.toString(values));
        }
    }

}
