package com.gcttc.gcttcrankings.infra.adapters.driven.rankings.sqldb;

import com.gcttc.gcttcrankings.domain.Match;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

import java.util.UUID;

@Entity
@Table(name = "match")
public class MatchJPAEntity {

    @Id
    private UUID id;
    @ManyToOne(cascade = CascadeType.ALL)
    private TournamentJPAEntity tournament;
    @ManyToOne
    private PlayerJPAEntity playerA;
    private Short playerAWonSets;
    @ManyToOne
    private PlayerJPAEntity playerB;
    private Short playerBWonSets;

    public MatchJPAEntity() {
    }

    public MatchJPAEntity(Match match) {
        this.id = match.getId();
        this.tournament = new TournamentJPAEntity(match.getTournament());
        this.playerA = new PlayerJPAEntity(match.getPlayerA());
        this.playerAWonSets = match.getPlayerAWonSets();
        this.playerB = new PlayerJPAEntity(match.getPlayerB());
        this.playerBWonSets = match.getPlayerBWonSets();
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public TournamentJPAEntity getTournament() {
        return tournament;
    }

    public void setTournament(TournamentJPAEntity tournament) {
        this.tournament = tournament;
    }

    public PlayerJPAEntity getPlayerA() {
        return playerA;
    }

    public void setPlayerA(PlayerJPAEntity playerA) {
        this.playerA = playerA;
    }

    public Short getPlayerAWonSets() {
        return playerAWonSets;
    }

    public void setPlayerAWonSets(Short playerAWonSets) {
        this.playerAWonSets = playerAWonSets;
    }

    public PlayerJPAEntity getPlayerB() {
        return playerB;
    }

    public void setPlayerB(PlayerJPAEntity playerB) {
        this.playerB = playerB;
    }

    public Short getPlayerBWonSets() {
        return playerBWonSets;
    }

    public void setPlayerBWonSets(Short playerBWonSets) {
        this.playerBWonSets = playerBWonSets;
    }

    public Match toMatch() {
        return new Match(this.id,
                this.tournament.toTournament(),
                this.playerA.toPlayer(),
                this.playerAWonSets,
                this.playerB.toPlayer(),
                this.playerBWonSets);
    }

}
