package com.gcttc.gcttcrankings.infra.adapters.driven.rankings.sqldb;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface TournamentJPARepository
        extends CrudRepository<TournamentJPAEntity, UUID> {

    List<TournamentJPAEntity> findAll();

    TournamentJPAEntity findFirstByOrderByDateDesc();

}
