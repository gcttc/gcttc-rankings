package com.gcttc.gcttcrankings.infra.adapters.driven.rankings.sqldb;

import com.gcttc.gcttcrankings.domain.Ranking;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

import java.time.LocalDate;
import java.util.UUID;

@Entity
@Table(name = "ranking")
public class RankingJPAEntity {

    @Id
    private UUID id;
    private LocalDate updatedAt;
    private UUID lastMatchID;
    @ManyToOne(cascade = CascadeType.ALL)
    private PlayerJPAEntity player;
    private Integer playerRankingBeforeMatch;
    private Integer playerRankingAfterMatch;
    private boolean currentRanking;

    public RankingJPAEntity() {
    }

    public RankingJPAEntity(Ranking ranking) {
        this.id = ranking.getId();
        this.updatedAt = ranking.getUpdatedAt();
        this.lastMatchID = ranking.getLastMatchID();
        this.player = new PlayerJPAEntity(ranking.getPlayer());
        this.playerRankingBeforeMatch = ranking.getPlayerRankingBeforeMatch();
        this.playerRankingAfterMatch = ranking.getPlayerRankingAfterMatch();
        this.currentRanking = ranking.isCurrentRanking();
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public LocalDate getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDate updatedAt) {
        this.updatedAt = updatedAt;
    }

    public PlayerJPAEntity getPlayer() {
        return player;
    }

    public void setPlayer(PlayerJPAEntity player) {
        this.player = player;
    }

    public Integer getPlayerRankingBeforeMatch() {
        return playerRankingBeforeMatch;
    }

    public Integer getPlayerRankingAfterMatch() {
        return playerRankingAfterMatch;
    }

    public boolean isCurrentRanking() {
        return currentRanking;
    }

    public void setCurrentRanking(boolean currentRanking) {
        this.currentRanking = currentRanking;
    }

    public Ranking toRanking() {
        return Ranking.buildRankingAs(this.id,
                this.updatedAt,
                this.lastMatchID,
                this.player.toPlayer(),
                this.getPlayerRankingBeforeMatch(),
                this.getPlayerRankingAfterMatch(),
                this.currentRanking);
    }

}
