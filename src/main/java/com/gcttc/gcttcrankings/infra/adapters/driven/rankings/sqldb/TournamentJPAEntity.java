package com.gcttc.gcttcrankings.infra.adapters.driven.rankings.sqldb;

import com.gcttc.gcttcrankings.domain.Tournament;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "tournament")
public class TournamentJPAEntity {

    @Id
    private UUID id;
    @Column(length = 200)
    private String name;
    @Column(length = 200)
    private String organizer;
    private LocalDate date;
    @OneToMany(mappedBy = "tournament", fetch = FetchType.EAGER)
    private List<MatchJPAEntity> matches;

    public TournamentJPAEntity() {
    }

    public TournamentJPAEntity(Tournament tournament) {
        this.id = tournament.getId();
        this.name = tournament.getName();
        this.organizer = tournament.getOrganizer();
        this.date = tournament.getDate();
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrganizer() {
        return organizer;
    }

    public void setOrganizer(String organizer) {
        this.organizer = organizer;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public List<MatchJPAEntity> getMatches() {
        return matches;
    }

    public void setMatches(List<MatchJPAEntity> rankings) {
        this.matches = rankings;
    }

    public Tournament toTournament() {
        return new Tournament(this.id,
                this.name,
                this.organizer,
                this.date);
    }

}
