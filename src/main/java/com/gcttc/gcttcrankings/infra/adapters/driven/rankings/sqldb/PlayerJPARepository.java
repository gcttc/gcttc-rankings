package com.gcttc.gcttcrankings.infra.adapters.driven.rankings.sqldb;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface PlayerJPARepository
        extends CrudRepository<PlayerJPAEntity, UUID> {

    List<PlayerJPAEntity> findAll();

    Optional<PlayerJPAEntity> findByNameAndLastname(String name, String lastname);

}
