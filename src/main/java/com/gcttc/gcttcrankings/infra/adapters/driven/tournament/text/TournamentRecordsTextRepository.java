package com.gcttc.gcttcrankings.infra.adapters.driven.tournament.text;

import com.gcttc.gcttcrankings.domain.PlayerRecord;
import com.gcttc.gcttcrankings.domain.TournamentRecord;
import com.gcttc.gcttcrankings.domain.exceptions.TournamentLoadException;
import com.gcttc.gcttcrankings.domain.ports.driven.TournamentRecordsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Stream;

@Component
public class TournamentRecordsTextRepository
        implements TournamentRecordsRepository {

    private final String tournamentsDirectory;

    private static final Logger LOGGER = LoggerFactory.getLogger(TournamentRecordsTextRepository.class);

    public TournamentRecordsTextRepository(@Value("${process.tournaments-directory:data/tournaments}") String tournamentsDirectory) {
        this.tournamentsDirectory = tournamentsDirectory;
    }

    @Override
    public List<TournamentRecord> loadAllTournamentRecords() {

        List<TournamentRecord> loadedTournamentRecords;

        LOGGER.info("Starting Tournament Records load process from directory <{}>...", tournamentsDirectory);

        try (Stream<Path> stream = Files.list(Paths.get(tournamentsDirectory)).sorted()) {
            loadedTournamentRecords =
                    stream.filter(file -> !Files.isDirectory(file) &&
                            !file.toFile().isHidden() &&
                            file.toFile().getName().endsWith(".txt")).map(this::loadTournamentRecordFromFile).toList();
        } catch (IOException e) {
            throw new TournamentLoadException(e);
        }

        LOGGER.info("Finishing Tournament Records load process. Total of files read: {}",
                loadedTournamentRecords.size());

        return loadedTournamentRecords;

    }

    private TournamentRecord loadTournamentRecordFromFile(Path file) {

        TournamentRecord tournament;

        LocalDate tournamentDate;

        LOGGER.info("Processing file <{}>...", file);

        tournamentDate =
                LocalDate.parse(file.toFile().getName().substring(0, 8), DateTimeFormatter.ofPattern("yyyyMMdd"));

        try (BufferedReader br = new BufferedReader(new FileReader(file.toFile()))) {

            String tournamentName = br.readLine();

            String tournamentOrganizer = br.readLine();

            tournament = new TournamentRecord(tournamentName, tournamentOrganizer, tournamentDate);

            String line;
            do {
                line = br.readLine();
            } while (!line.equals("#Partidos"));


            while ((line = br.readLine()) != null) {

                if (line.isBlank() || line.startsWith("#")) continue;

                String[] values = line.split(",");

                PlayerRecord playerA = new PlayerRecord(values[1].trim(), values[0].trim());
                Short playerAWonSets = Short.valueOf(values[2].trim());
                PlayerRecord playerB = new PlayerRecord(values[4].trim(), values[3].trim());
                Short playerBWonSets = Short.valueOf(values[5].trim());

                tournament.registerMatch(playerA, playerAWonSets, playerB, playerBWonSets);

            }

        } catch (IOException e) {
            throw new TournamentLoadException(e);
        }

        return tournament;
    }

}
