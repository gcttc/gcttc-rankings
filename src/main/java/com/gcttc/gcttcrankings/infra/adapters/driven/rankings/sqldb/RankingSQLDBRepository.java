package com.gcttc.gcttcrankings.infra.adapters.driven.rankings.sqldb;

import com.gcttc.gcttcrankings.domain.Match;
import com.gcttc.gcttcrankings.domain.Player;
import com.gcttc.gcttcrankings.domain.Ranking;
import com.gcttc.gcttcrankings.domain.Tournament;
import com.gcttc.gcttcrankings.domain.ports.driven.RankingRepository;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;

@Component
public class RankingSQLDBRepository
        implements RankingRepository {
    private final RankingJPARepository rankingJPARepository;
    private final PlayerJPARepository playerJPARepository;
    private final MatchJPARepository matchJPARepository;
    private final TournamentJPARepository tournamentJPARepository;

    public RankingSQLDBRepository(RankingJPARepository rankingJPARepository,
                                  PlayerJPARepository playerJPARepository,
                                  MatchJPARepository matchJPARepository,
                                  TournamentJPARepository tournamentJPARepository) {
        this.rankingJPARepository = rankingJPARepository;
        this.playerJPARepository = playerJPARepository;
        this.matchJPARepository = matchJPARepository;
        this.tournamentJPARepository = tournamentJPARepository;
    }

    @Override
    public void saveRankings(List<Ranking> rankingEntries) {
        rankingJPARepository.saveAll(rankingEntries.stream().map(RankingJPAEntity::new).toList());
    }

    @Override
    public void saveRanking(Ranking ranking) {
        rankingJPARepository.save(new RankingJPAEntity(ranking));
    }

    @Override
    public List<Ranking> findAllRankings() {
        return rankingJPARepository.findAll().stream().map(RankingJPAEntity::toRanking).toList();
    }

    @Override
    public Long countAllRankings() {
        return rankingJPARepository.count();
    }

    @Override
    public Ranking findLastRankingByPlayerId(UUID playerID) {
        return rankingJPARepository.findByPlayerId(playerID)
                                   .stream()
                                   .reduce((first, second) -> second)
                                   .map(RankingJPAEntity::toRanking)
                                   .orElse(null);
    }

    @Override
    public List<Ranking> findRankingsByPlayerId(UUID playerID) {
        return rankingJPARepository.findByPlayerId(playerID).stream().map(RankingJPAEntity::toRanking).toList();
    }

    @Override
    public Ranking findPairRankingByRanking(Ranking ranking) {
        return rankingJPARepository.findByLastMatchID(ranking.getLastMatchID())
                                   .stream()
                                   .filter(r -> !r.getId().equals(ranking.getId()))
                                   .map(RankingJPAEntity::toRanking)
                                   .findFirst()
                                   .orElse(null);
    }

    @Override
    public void saveTournament(Tournament tournament) {
        tournamentJPARepository.save(new TournamentJPAEntity(tournament));
    }

    @Override
    public List<Tournament> findAllTournaments() {
        return tournamentJPARepository.findAll().stream().map(TournamentJPAEntity::toTournament).toList();
    }

    @Override
    public Long countAllTournaments() {
        return tournamentJPARepository.count();
    }

    @Override
    public void saveMatch(Match match) {
        matchJPARepository.save(new MatchJPAEntity(match));
    }

    @Override
    public Long countAllMatches() {
        return matchJPARepository.count();
    }

    @Override
    public List<Match> findMatchesByTournament(UUID tournamentID) {
        return matchJPARepository.findByTournamentId(tournamentID).stream().map(MatchJPAEntity::toMatch).toList();
    }

    @Override
    public Match findMatchById(UUID matchID) {
        return matchJPARepository.findById(matchID).map(MatchJPAEntity::toMatch).orElse(null);
    }

    @Override
    public void savePlayer(Player player) {
        playerJPARepository.save(new PlayerJPAEntity(player));
    }

    @Override
    public Long countAllPlayers() {
        return playerJPARepository.count();
    }

    @Override
    public List<Player> findAllPlayers() {
        return playerJPARepository.findAll().stream().map(PlayerJPAEntity::toPlayer).toList();
    }

    @Override
    public Player findPlayerByNameAndLastname(String name, String lastname) {
        return playerJPARepository.findByNameAndLastname(name, lastname).map(PlayerJPAEntity::toPlayer).orElse(null);
    }

    public void truncate() {
        this.rankingJPARepository.deleteAll();
        this.playerJPARepository.deleteAll();
        this.matchJPARepository.deleteAll();
        this.tournamentJPARepository.deleteAll();
    }
}
