package com.gcttc.gcttcrankings.infra.adapters.driven.rankings.sqldb;

import com.gcttc.gcttcrankings.domain.Player;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "player")
public class PlayerJPAEntity {

    @Id
    private UUID id;
    @Column(length = 100)
    private String name;
    @Column(length = 100)
    private String lastname;
    @OneToMany(mappedBy = "player")
    private List<RankingJPAEntity> rankings;

    public PlayerJPAEntity() {
    }

    public PlayerJPAEntity(Player player) {
        this.id = player.getId();
        this.name = player.getName();
        this.lastname = player.getLastname();
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public List<RankingJPAEntity> getRankings() {
        return rankings;
    }

    public void setRankings(List<RankingJPAEntity> rankings) {
        this.rankings = rankings;
    }

    public Player toPlayer() {
        return new Player(this.id, this.name, this.lastname);
    }

}
