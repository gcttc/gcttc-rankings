package com.gcttc.gcttcrankings;

import com.gcttc.gcttcrankings.domain.ports.driven.ExportedRankingsRepository;
import com.gcttc.gcttcrankings.domain.ports.driven.InitialRankingRepository;
import com.gcttc.gcttcrankings.domain.ports.driven.RankingRepository;
import com.gcttc.gcttcrankings.domain.ports.driven.TournamentRecordsRepository;
import com.gcttc.gcttcrankings.domain.ports.driver.ForExportingRankings;
import com.gcttc.gcttcrankings.domain.ports.driver.ForLoadingInitialRanking;
import com.gcttc.gcttcrankings.domain.ports.driver.ForLoadingTournaments;
import com.gcttc.gcttcrankings.domain.services.InitialRankingLoader;
import com.gcttc.gcttcrankings.domain.services.RankingExporter;
import com.gcttc.gcttcrankings.domain.services.TournamentRecordLoader;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class GcttcRankingsApplication {

    public static void main(String[] args) {
        SpringApplication.run(GcttcRankingsApplication.class, args);
    }

    @Bean
    public ForLoadingInitialRanking forLoadingInitialRanking(InitialRankingRepository initialRankingRepository,
                                                             RankingRepository rankingRepository) {
        return new InitialRankingLoader(initialRankingRepository, rankingRepository);
    }

    @Bean
    public ForLoadingTournaments forLoadingTournaments(TournamentRecordsRepository tournamentRecordsRepository,
                                                       RankingRepository rankingRepository) {
        return new TournamentRecordLoader(tournamentRecordsRepository, rankingRepository);
    }

    @Bean
    public ForExportingRankings forExportingRankings(RankingRepository rankingRepository,
                                                     ExportedRankingsRepository exportedRankingsRepository) {
        return new RankingExporter(rankingRepository, exportedRankingsRepository);
    }

}
