#!/bin/sh
echo "=========== Integration Test Script ========================"

ls target/gcttc-rankings*.jar

git clone --depth 1 https://gitlab.com/gcttc/gcttc-rankings-data.git

cp target/gcttc-rankings*.jar gcttc-rankings-data

cd gcttc-rankings-data || exit

mv gcttc-rankings*.jar gcttc-rankings.jar

echo "Executing Ranking calculation"
java -jar gcttc-rankings.jar || exit

echo "Result files"
ls -l data/output || exit

echo "Result file content (first 25 lines):"
jq '.' data/output/rankings.json | head -n 25 || exit

echo "Result file ranking count:"
jq '.rankings | length' data/output/rankings.json || exit

echo "==========================================================="