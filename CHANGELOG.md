# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/), and this project adheres
to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.3.0]

### Added

- Added match and opponent metadata to the ranking report file. [Issue #7](https://gitlab.com/gcttc/gcttc-rankings/-/issues/7)

## [0.2.3]

### Fixed

- NPE when reading non existent players from ranking repository.

## [0.2.2]

### Fixed

- Exception when reading tournament directory with non-txt files.

## [0.2.1]

### Fixed

- Exception when reading tournament files with empty or blank lines in the match section. [Issue #4](https://gitlab.com/gcttc/gcttc-rankings/-/issues/4)
- InvalidTotalNumberOfSetsException message. Now it shows context match data. [Issue #5](https://gitlab.com/gcttc/gcttc-rankings/-/issues/5)
- InvalidTotalNumberOfSetsException validation logic. Now it supports results like 3-1. [Issue #6](https://gitlab.com/gcttc/gcttc-rankings/-/issues/6)

## [0.2.0]

### Added

- Added ranking change logs.

## [0.1.6]

### Fixed

- CHANGELOG information.

## [0.1.5]

### Changed

- The initial ranking file field delimiter is now a comma (`,`).

## [0.1.4]

### Fixed

- Ranking report ranking's date as ISO8601.

## [0.1.3]

### Fixed

- Ranking report date as ISO8601.

## [0.1.2]

### Added

- Support for exporting ranking's last update date.

## [0.1.1]

### Fixed

- Integration tests regarding version tag for download and execution.

### Added

- This `CHANGELOG.md` file. 

## [0.1.0] - 2024-05-07

### Added

- First production-ready version of the tool. 

[Unreleased]: https://gitlab.com/gcttc/gcttc-rankings/-/compare/gcttc-rankings-0.1.1...HEAD
[0.1.1]: https://gitlab.com/gcttc/gcttc-rankings/-/tags/gcttc-rankings-0.1.1
[0.1.0]: https://gitlab.com/gcttc/gcttc-rankings/-/tags/gcttc-rankings-0.1.0